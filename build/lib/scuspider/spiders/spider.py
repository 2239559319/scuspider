import scrapy
from ..items import ScuspiderItem

class MainSpider(scrapy.Spider):
    name = "scu-jiangxuejin"

    def start_requests(self):
        urls = [
            'https://xsc.scu.edu.cn/WebSite/StuHelp/StuHelp/BZMDshow?3dfQ0JeIJlt9Labr4OSA0378FtWfGlPht9gP97gYwg5f1KUwCuBVd7Mqvq2cv3qU3HM/Elq/P4gsxtfd2J4uyg==.shtml',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        baseUrl = 'https://xsc.scu.edu.cn'

        l = response.xpath('//tr[position() > 1]')
        for item in l:
            id = item.xpath('td[1]/text()').get()
            name = item.xpath('td[2]/text()').get()
            benke = item.xpath('td[3]/text()').get()
            grade = item.xpath('td[4]/text()').get()
            majar = item.xpath('td[5]/text()').get()
            ins = item.xpath('td[6]/text()').get()
            price = item.xpath('td[7]/text()').get()
            project = item.xpath('td[8]/text()').get()
            xuenian = item.xpath('td[9]/text()').get()

            if name == '张懿':
                i = ScuspiderItem(id=id, name=name, benke=benke, grade=grade, majar=majar, ins=ins, price=price, project=project,xuenian=xuenian)
                yield i

        links = response.xpath('//span[@class="xg-page-btn"]')
        for link in links:
            t = link.xpath('text()').get()
            if t == '>>':
                nextLink = link.xpath('../@href').get()
                if nextLink:
                    yield scrapy.Request(url=f'{baseUrl}{nextLink}', callback=self.parse)
