# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScuspiderItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    id = scrapy.Field()
    name = scrapy.Field()
    benke = scrapy.Field()
    grade = scrapy.Field()
    majar = scrapy.Field()
    ins = scrapy.Field()
    price = scrapy.Field()
    project = scrapy.Field()
    xuenian = scrapy.Field()

class LinkItem(scrapy.Item):
    id = scrapy.Field()
    owner = scrapy.Field()
    link = scrapy.Field()