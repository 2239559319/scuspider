import scrapy
from ..items import LinkItem
from urllib.parse import parse_qs

class LinkSpider(scrapy.Spider):
    name = "scu-jiaowuchu-link"

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36',
        'referer': 'https://jwc.scu.edu.cn/'
    }

    def start_requests(self):
        urls = [
            'https://jwc.scu.edu.cn/tzgg.htm',
        ]

        for i in range(181):
            num = i + 1
            url = f'https://jwc.scu.edu.cn/tzgg/{num}.htm'
            urls.insert(0, url)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, headers=self.headers)

    def parse(self, response):

        baseUrl = 'https://jwc.scu.edu.cn/'
        links = response.css('div[class="tz-list"] ul a::attr(href)').getall()
        for link in links:
            _link = link
            if link.startswith('../'):
                _link = link[3:]

            url = baseUrl + _link
            yield scrapy.Request(url=url, callback=self.parseMain, headers=self.headers)


    def parseMain(self, response):

        url = response.css('.fjxz a::attr(href)').get()

        if url is not None:
            query_params = parse_qs(url)
            owner = query_params.get('owner', [''])[0]
            wbfileid = query_params.get('wbfileid', [''])[0]
            item = LinkItem(id=wbfileid, owner=owner, link=url)
            yield item


